from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    # AutomobileVOEncoder,
    CustomerEncoder,
    SalesEncoder,
    SalespersonEncoder,
)

from .models import (
    AutomobileVO,
    Customer,
    Salesperson,
    Sale,
)


# Create your views here.
# Lists Customers and Creates a customer, no foreign keys
@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Exception as ex:
            response = JsonResponse(
                {"Message": "Could not create the customer,"
                 "how does that even happen, there,"
                 "are no foreign keys here...happened count: 1"}
            )
            response.status_code = 400
            print(ex)
            return response


# Deletes customer
@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Customer.objects.get(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0},
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "DNE or already deleted"})


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Exception as ex:
            response = JsonResponse(
                {"Message": "Could not create the salesperson,"
                 "curious as there are no foreign keys here"}
            )
            response.status_code = 400
            print(ex)
            return response


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, employee_id):
    if request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.get(employee_id=employee_id).delete()
            return JsonResponse(
                {"deleted": count > 0},
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "DNE or already deleted"})


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        print(object)
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            employee_id = content["employee_id"]
            salesperson = Salesperson.objects.get(id=employee_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Employee not in database"},
                status=400
            )
        try:
            vin = content["vin"]
            auto = AutomobileVO.objects.get(pk=vin)
            content["auto"] = auto
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "VIN does not match a real car"},
                status=400
                )
        try:
            phone_number = content["phone_number"]
            customer = Customer.objects.get(pk=phone_number)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not in database"},
                status=400
            )
        except Exception as ex:
            response = JsonResponse(
                {"message": "could not create the sale"},
                status=400
            )
            response.status_code = 400
            print(ex)
            return response

        sale = Sale.object.create(**content)
        sale.automobile.sold = True
        sale.automobile.save()
        return JsonResponse(
            sale,
            encoder=SalesEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Sale.objects.get(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0},
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale DNE or already deleted"})
