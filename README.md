# CarCar

Team:

* Person 1 - Which microservice? Spencer Privette -Sales
* Person 2 - Daniel Gay -services


## How to Run this App
 - Put instructions to build and run this app here
docker volume create beta-data
docker-compose build
docker-compose up

## Diagram
 - Put diagram here

## API Documentation


### URLs and Ports
 - Put URLs and ports for services here

## Design
1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<respository.url.here>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

-![Alt text](image.png)
-in file design chart.png

for the most part for ease of user manufacturer has been replaced with make not including APIs.
## Design 
CarCar 
is made of 3 microservices wich interact to give react bood data
-inventory
-sales
-services

# integration
sales and service microservices track with a poller to track the autos made inventory service. in services so we know who has purchased a car from us to give them BIP treatment for service appointments.
## Service microservice
___________________________________________
api testing
___________________________________________
techs

GET/list techs - http://localhost:8080/api/technicians/
GET/detailes-http://localhost:8080/api/technicians/ID/
GETdelete tech- http://localhost:8080/api/technicians/ID/
POST/create tech-http://localhost:8080/api/technicians/

___________________________________________
appointments

GET/appointments list all-http://localhost:8080/api/appointments/
GET/ active appointments-http://localhost:8080/api/appointments_act/
PUT- change appointment-http://localhost:8080/api/appointments/ID/
POST/create-http://localhost:8080/api/appointments/
DELETE/delete-http://localhost:8080/api/appointments/1/
GET/appointment by vin-http://localhost:8080/api/appointments/2222222/

__________________________________________
automotiveVO

GET/automotiveVO list-http://localhost:8080/api/AutomobileVO/


3 model s
-Technician will track the techs first and last name as well as employee id this will have to be unique no duplicates
-AutomobileVO will track the vin and if its sold-could not get polling working per message/question in slack
this is used in the front end to check for VIP
work around to run api request from react 
-Appointment appointment time reason for appointment status vin and
customer. as a foreign key we hav it linked to the technician model

user will be able to make and set appointment search a vins history sold from us status any services it has had with us and track current status of services.
 add or remove helpfull technichian staff.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

localhost:3000/api/

CRD Endpoints: (since we weren't asked to be able to update, although in retrospect
that would have made changing Sales to True easier)

___________________________________________
customers
sample insomnia
{
	"first_name": "john",
	"last_name": "smith2",
	"address": "123456 test ave Denver, CO",
	"phone_number": "2234567892"
}

GET Customers - http://localhost:8090/api/customers/
POST customers - http://localhost:8090/api/customers/
DEL customers customer - http://localhost:8090/api/customers/ID/

___________________________________________
salespeople
sample insomnia:
{
	"first_name": "Spencer1",
	"last_name": "P2",
	"employee_id": 1234
}

GET salespeople - http://localhost:8090/api/salespeople/
POST/ salesperson - http://localhost:8090/api/salespeople/
DEL salesperson - http://localhost:8090/api/salespeople/ID/

___________________________________________
sales
couldn't quite get this part working, this was the goal though.
{
	"automobile": "1ab",
	"salesperson": "1234",
	"customer": "1",
	"price":"800"
}


GET sales - http://localhost:8090/api/appointments/
POST sales - http://localhost:8090/api/appointments/
GET/ active appointments-http://localhost:8090/api/appointments_act/
PUT- change appointment-http://localhost:8090/api/appointments/ID/



## Value Objects
- Service API


- Sales API: the Sales API uses the Automobile value object. It pulls a list of attributes from the Automobile database in the inventory API

AutomobileVO
- Has a VIN and href. That href is used to pull the rest of Models and Manufacturing info from the inventory API. VIN is unique and the form will not submit if a used VIN is entered.

Salesperson
- Has employee_id, first + last name, and employee ID.

Customer 
- First + last name + address + phone number. The phone number is used as
the unique identifier, so the same number cannot be entered twice into the database.

Sale
-the value object that ties all 3 classes together as foreign keys + price to make up
arguably the most important part of the project. They will only show up in the list if they are sold.



List Customer http://localhost:3000/customer/
Add CustomerForm http://localhost:3000/customer/new/
List CustomerForm http://localhost:3000/customer/
Add CustomerForm http://localhost:3000/customer/new/
List Sales http://localhost:3000/sales/
Add Sale http://localhost:3000/sale/new/
List Salespeople http://localhost:3000/salespeople/
Add Salesperson http://localhost:3000/salespeople/new/
Salesperson History http://localhost:3000/salesperson/history
