import React, { useState } from 'react';

function MakeForm() {
    const [ formData, setMake ] = useState({name:''})
   
    const handleMake = (event) => {
        const value = event.target.value;
        setMake(value);
      }

const handleSubmit = async (event) => {
    event.preventDefault();
  

    const url = `http://localhost:8100/api/manufacturers/`;

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(url, fetchConfig);
    console.log("post", response)

    if (response.ok) {
        setMake({name:''})
    }
}


return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create new Manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleMake} placeholder="Make" required type="text" name="Make" id="Make" className="form-control" value={formData.make} />
              <label htmlFor="Make">Manufacturer</label>
            </div>          
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
)

}

export default MakeForm;
