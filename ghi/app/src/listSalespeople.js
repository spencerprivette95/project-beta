import { useState, useEffect } from "react";


function ListSalespeople() {
    const [salespeople, setSalespeople] = useState([])

    const fetchData = async() => {    
    const response= await fetch('http://localhost:8090/api/salespeople')
        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salespeople)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
        <h1>List of Salespeople</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Employee id</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.id}>
                                <td>{ salesperson.first_name }</td>
                                <td>{ salesperson.last_name }</td>
                                <td>{ salesperson.employee_id }</td>
                            </tr>
                        );
                        })}
            </tbody>
        </table>
        </>
    )
    
}



export default ListSalespeople
