import { useState, useEffect } from "react";

function NewCustomer() {

    const[formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    })


    const handleSubmit = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body :JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            })
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    return (
        <>
        <div>
            <h1>New Customer</h1>
            <form id="create-customer-form" onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="first_name">First Name</label>
                    <input onChange={handleFormChange} value={formData.first_name} type="text" name="first_name" id="first_name" placeholder="Enter the new color" required/>
                </div>
                <div>
                    <label htmlFor="last_name">Last name</label>
                    <input onChange={handleFormChange} value={formData.last_name} type="text" name="last_name" id="last_name" placeholder="Car year goes here" required/>
                </div>
                <div>
                    <label htmlFor="address">Address</label>
                    <input onChange={handleFormChange} value={formData.address} type="text" name="address" id="address" placeholder="Address" required />
                </div>
                <div>
                    <label htmlFor="phone_number">Phone number</label>
                    <input onChange={handleFormChange} value={formData.phone_number} type="number" name="phone_number" id="phone_number" required/>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </>
    )
}
export default NewCustomer
