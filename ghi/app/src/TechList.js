import React, { useEffect, useState} from "react";
import { Link } from 'react-router-dom';
const TechList = () =>{
    const [TechsList, setList] = useState([]);
    async function Gettech(){
        
        try{
            
        const response = await fetch("http://localhost:8080/api/technicians/");
            if (response.ok){
                const data =  await response.json();
                setList(data.technician);
            }
        }catch (e) {
            console.error(e);
        }
    }
useEffect(() => {
    Gettech();
  }, []);
  return(
    <>
  <table class="table table-dark table-striped">
    <thead>
        <tr> 
            <td>first name</td>
            <td>last name</td>
            <td>employee Id</td>
            
        </tr>
    </thead>
    <tbody>
        {TechsList.map(tech => {
            return(
                <tr key={tech.employee_id}>
                <td>{tech.first_name}</td>
                <td>{tech.last_name}</td>
                <td>{tech.employee_id}</td>      
                </tr>          
            )
        })}
    </tbody>
</table>
<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
    <Link to="/techs/new" className="btn btn-primary btn-lg px-4 gap-3">Add a new technichian</Link>
</div>
    </>
  );
  }

export default TechList;