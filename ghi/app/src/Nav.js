import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item'>
              <NavLink className="nav-link" to="/automobiles">Automobiles List</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="/sales">List All Sales</NavLink>
            </li>

            <li className='nav-item'>
            <NavLink className="nav-link" to="appointments/active">Active appointments </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="Make">Manufacturer</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="Make/new">AddManufacturer </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="model">Model </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="model/new">AddModel </NavLink>
            </li>   

            <li className='nav-item'>
              <NavLink className="nav-link" to="techs">technicians </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="techs/new">Addtechnicians </NavLink>
            </li>


            <li className='nav-item'>
              <NavLink className="nav-link" to="appointments/new">Addappointments </NavLink>
            </li>
  
            <li className='nav-item'>
              <NavLink className="nav-link" to="appointments/vin">service history </NavLink>
            </li>    

            <li className='nav-item'>
             <NavLink className="nav-link" to="/automobiles/new">Create an Automobile</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="/customers/new">New Customer</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="/salespeople/new">New Salesperson</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="/salespeople/history">Salesperson history</NavLink>
            </li>

            <li className='nav-item'>
              <NavLink className="nav-link" to="/sales/new">Record New Sale</NavLink>
           </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
