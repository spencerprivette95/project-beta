import React, {useEffect, useState} from "react";

function NewAutoForm() {
    const [models, setModels] = useState([])
    
    const[formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        sold: false,
        model_id: '',
    })
    
    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";
        const modelsResponse = await fetch(url);
        if (modelsResponse.ok) {
            const modelsData= await modelsResponse.json();
            setModels(modelsData.models);
        }
    }

    useEffect(() => {
        fetchData();
    },[]    
    )
    
    const handleSubmit = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/'        
        const fetchConfig = {
            method: "post",
            body :JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id:'',
            })
        }
        
    };
    
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    return(
        <>
            <div>
                <h1>New Automobile</h1>
                <form id="create-auto-form" onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="color">Color</label>
                        <input onChange={handleFormChange} value={formData.color} type="text" name="color" id="color" placeholder="Enter the new color" required/>
                    </div>
                    <div>
                        <label htmlFor="year">Year</label>
                        <input onChange={handleFormChange} value={formData.year} type="number" name="year" id="year" placeholder="Car year goes here" required/>
                    </div>
                    <div>
                        <label htmlFor="vin">VIN</label>
                        <input onChange={handleFormChange} value={formData.vin} type="" name="vin" id="vin" placeholder="Unique VIN" required/>
                    </div>
                    <div>
                        <label htmlFor="model_id">Model</label>
                        <select onChange={handleFormChange} value={formData.model_id} className="form-select" name="model_id" id="model_id"  required>
                            <option value="">Choose a model</option>
                            {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>  {model.manufacturer.name + " " + model.name}  </option>
                                )
                            }
                            )}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        <p>This is a create automobile page.</p>
        </>
    )
}

export default NewAutoForm
