import { useState, useEffect } from "react";

function ListCustomers() {
    const [customers, setCustomers] = useState([])

    const fetchData =async() => {
    const response = await fetch('http://localhost:8090/api/customers')
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);
    return(
        <>
        <h1>List of Customers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customers.map(customer => {
                        return (
                            <tr key={customer.id}>
                                <td>{ customer.first_name }</td>
                                <td>{ customer.last_name }</td>
                                <td>{ customer.address }</td>
                                <td>{ customer.phone_number }</td>
                            </tr>
                        );
                        })}
            </tbody>
        </table>
        </>
    )
} 


export default ListCustomers
