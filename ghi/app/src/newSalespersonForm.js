import { useState, useEffect } from "react";

function NewSalesperson() {

    const[formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })


    const handleSubmit = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: "post",
            body :JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            })
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    return (
        <>
        <div>
            <h1>New Salesperson</h1>
                <div>
                <form id="create-salesperson-form" onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="first_name">First Name</label>
                        <input onChange={handleFormChange} value={formData.first_name} type="text" name="first_name" id="first_name" placeholder="First name" required/>
                    </div>
                    <div>
                        <label htmlFor="last_name">Last Name</label>
                        <input onChange={handleFormChange} value={formData.last_name} type="text" name="last_name" id="last_name" placeholder="Last Name" required/>
                    </div>
                    <div>
                        <label htmlFor="employee_id">Employee ID</label>
                        <input onChange={handleFormChange} value={formData.employee_id} type="text" name="employee_id" id="employee_id" placeholder="Employee ID" required/>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
        </div>
        <p>This is a create automobile page.</p>
        </>
    )
}



export default NewSalesperson
