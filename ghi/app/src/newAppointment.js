import React, { useEffect, useState } from 'react';

function NewAppointment() {
    const [ makeList, settech ] = useState([])
    const [ formData, setFormData ] = useState({
        date_time: '',
        reason : '',
        status : 'unfinished',
        vin : '',
        customer :'',
        technician_id :''
    })
    const fetchData = async () => {
        const url = `http://localhost:8080/api/technicians/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            settech(data.technician);
            console.log(data.technician)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

     
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;    
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
const handleSubmit = async (event) => {
    event.preventDefault();  

    const url = `http://localhost:8080/api/appointments/`;
    
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(url, fetchConfig);
    console.log(formData)
    console.log("post>>>>>>>>>>", response)

    if (response.ok) {
        setFormData({
        date_time: '',
        reason : '',
        status : 'unfinished',
        vin : '',
        customer :'',
        technician_id :''
        })
    }
}



return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>add a new appointment</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.date_time} placeholder="date_time" required type="date" name="date_time" id="date_time" className="form-control"  />
              <label htmlFor="date_time">date and time of NewAppointment</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"  />
              <label htmlFor="reason">reason</label>
            </div>                 
            {/* <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.status} placeholder="status" required type="text" name="status" id="status" className="form-control"  />
              <label htmlFor="status">status</label>
            </div>  */}
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"  />
              <label htmlFor="vin">vin</label>
            </div> 
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.customer} placeholder="vin" required type="text" name="customer" id="customer" className="form-control"  />
              <label htmlFor="customer">customer</label>
            </div> 
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.id} required name="technician_id" id="technician_id" className="form-select">
                <option value="">Choose a technician</option>
                {makeList.map(tech => {
                  return (
                    <option key={tech.id} value={tech.id}>{tech.last_name} {tech.first_name}</option>
                  )
                })}
              </select>
            </div>        
          
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
)

}

export default NewAppointment;
