from django.urls import path
from .views import (
    api_Technicians,
    api_Technician,
    api_Appointments,
    api_Appointments_act,
    api_Appoint_Vin,
    api_Appointment,
    api_AutomobileVO,
)

urlpatterns = [
    path("technicians/", api_Technicians, name="api_Technicians"),
    path("technicians/<int:pk>/", api_Technician, name="api_Technician"),
    path("appointments/", api_Appointments, name="api_Appointments"), 
    path("appointments_act/",
         api_Appointments_act,
         name="api_Appointments_act"),
    path("appointment/<int:pk>/", api_Appointment, name="api_Appointment"),
    path("appointments/<str:vin>/", api_Appoint_Vin, name="api_Appoint_Vin"),
    path("AutomobileVO/", api_AutomobileVO, name="api_AutomobileVO"),
]