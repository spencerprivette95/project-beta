from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=200, unique=True)
    
    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    sold = models.CharField(max_length=100)


class Appointment (models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100, null=True)
    status = models.CharField(max_length=100, null=True)
    vin = models.CharField(max_length=200, null=True)
    customer = models.CharField(max_length=100, null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="Technician",
        on_delete=models.CASCADE,
        null=True
    )

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

