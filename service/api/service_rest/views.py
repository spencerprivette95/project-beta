from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
        ]     
    

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
        ]
    

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }


@require_http_methods(["GET"])
def api_AutomobileVO(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVO": automobileVO},
            encoder=AutomobileVOEncoder,
        )
   

@require_http_methods(["GET", "POST"])
def api_Appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    

@require_http_methods(["GET"])
def api_Appointments_act(request):
    if request.method == "GET":
        appointment = Appointment.objects.filter(status="unfinished")
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_Appoint_Vin(request, vin):
    if request.method == "GET":
        appointment = Appointment.objects.filter(vin=vin)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    

@require_http_methods(["GET", "PUT", "DELETE"])
def api_Appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.filter(id=pk)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()

        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        
        return JsonResponse(
            appointment,
            AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_Technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_Technician(request, pk):
    if request.method == "GET":
        technician = Technician.objects.filter(id=pk)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()

        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        
        return JsonResponse(
            technician,
            TechnicianDetailEncoder,
            safe=False,
        )
